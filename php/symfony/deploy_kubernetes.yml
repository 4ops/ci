# Pipeline jobs for working with Kubernetes

# --- Templates

# 1. Setup kubectl config
# 2. Create/update namespace and image pull secret
# 3. Deploy application
# 4. Cleanup credentials
.deploy_kubernetes:
  stage: Deploy
  image:
    name: alpine/helm:3.0.1
    entrypoint: ["/bin/sh", "-c"]
  variables:
    IMAGE_PULL_SECRET_NAME: '${APPNAME}-repo-cred'
    HELM_RELEASE: '${APPNAME}-${CI_ENVIRONMENT_SLUG}'
    CI_REGISTRY_USER: '${APPNAME}'
    CI_REGISTRY_PASSWORD: ${REGISTRY_TOKEN}
  before_script:
    # Print debug info
    - echo "KUBE_NAMESPACE - $KUBE_NAMESPACE"
    - echo "HELM_RELEASE - $HELM_RELEASE"
    - echo "IMAGE_PULL_SECRET_NAME - $IMAGE_PULL_SECRET_NAME"
    - echo "CI_REGISTRY_USER - $CI_REGISTRY_USER"
    - echo "CI_ENVIRONMENT_SLUG - $CI_ENVIRONMENT_SLUG"
    - echo "CI_ENVIRONMENT_URL - $CI_ENVIRONMENT_URL"

    # Install system dependencies
    - apk add --quiet curl

    # Download and install kubectl
    - export KUBECTL_VERSION="1.16.2"
    - export KUBECTL_URL="https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
    - curl -fsSLO "${KUBECTL_URL}"
    - chmod 0755 kubectl
    - mv kubectl /usr/local/bin/kubectl
    - kubectl version --short --client

    # Setup kube config
    - export KUBE_CA_CERT=$(mktemp)
    - echo "$KUBE_CA_PEM" > "$KUBE_CA_CERT"
    - kubectl config set-cluster k8s
      --certificate-authority="$KUBE_CA_CERT"
      --embed-certs=true
      --server="$KUBE_URL"
    - kubectl config set-credentials ci --token="$KUBE_TOKEN"
    - kubectl config set-context k8s-ci
      --cluster=k8s
      --user=ci
      --namespace="$KUBE_NAMESPACE"
    - kubectl config use-context k8s-ci

    # Check helm
    - helm version --client --short

    # Create namespace
    - kubectl get namespace ${KUBE_NAMESPACE} || kubectl create namespace ${KUBE_NAMESPACE}

    # Update imagePullSecret
    - kubectl create secret docker-registry ${IMAGE_PULL_SECRET_NAME}
      --namespace "$KUBE_NAMESPACE"
      --dry-run=true
      --docker-server=registry.gitlab.com
      --docker-username="$CI_REGISTRY_USER"
      --docker-password="$CI_REGISTRY_PASSWORD"
      --docker-email="$GITLAB_USER_EMAIL"
      --output=yaml | kubectl apply -f -

    # Setup defaults if empty
    - test -n "$HELM_RELEASE" || export HELM_RELEASE="${APPNAME:-$CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}"
    - test -n "$HELM_TIMEOUT" || export HELM_TIMEOUT="10m"

  after_script:
    - rm -f "$KUBECONFIG"
  script:
    # Deploy application
    - helm upgrade "$HELM_RELEASE" .chart
      --install
      --wait
      --timeout ${HELM_TIMEOUT}
      --namespace "$KUBE_NAMESPACE"
      --values ".chart/environments/${CI_ENVIRONMENT_SLUG}/values.yaml"
      --set environment="$CI_ENVIRONMENT_NAME"

.start_stage:
  environment:
    name: ${STAGE_ENVIRONMENT_NAME}
    url: ${STAGE_ENVIRONMENT_URL}
    on_stop: Uninstall stage

.stop_stage:
  script: helm uninstall ${HELM_RELEASE} --namespace ${KUBE_NAMESPACE}
  environment:
    name: ${STAGE_ENVIRONMENT_NAME}
    action: stop

.helm_rollback_command:
  script: helm uninstall ${HELM_RELEASE} --namespace ${KUBE_NAMESPACE}

# --- Default settings

variables:
  # Helm and kubectl settings
  APPNAME: ${CI_PROJECT_NAME}
  TAG: ${CI_PIPELINE_ID}
  KUBECONFIG: /tmp/kube-config.yaml
  HELM_TIMEOUT: 10m
  STAGE_ENVIRONMENT_NAME: stage
  PROD_ENVIRONMENT_NAME: prod

# --- Environments

.stage_environment:
  environment:
    name: ${STAGE_ENVIRONMENT_NAME}
    url: ${STAGE_ENVIRONMENT_URL}

.prod_environment:
  environment:
    name: ${PROD_ENVIRONMENT_NAME}
    url: ${PROD_ENVIRONMENT_URL}
