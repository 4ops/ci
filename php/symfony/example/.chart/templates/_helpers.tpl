{{/* vim: set filetype=mustache: */}}
{{/*
   --- Common app templates
*/}}
{{/* Define the name of the application. */}}
{{- define "app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* Define the fullname of the application */}}
{{- define "app.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/* Create chart name and version as used by the chart label */}}
{{- define "app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* Create list of default labels */}}
{{- define "app.labels" -}}
helm.sh/chart: {{ include "app.chart" . | quote }}
{{ include "app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
{{- range $key, $val := .Values.labels -}}
{{ printf "%s: %s" $key ($val | quote) }}
{{- end -}}
{{- end -}}

{{/* Selector labels */}}
{{- define "app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.name" . | quote }}
app.kubernetes.io/instance: {{ .Release.Name | quote }}
environment: {{ required "Resource must be labeled with actual enviroment name" .Values.environment | quote }}
{{- end -}}

{{- define "app.environment" -}}
- name: NAMESPACE
  valueFrom:
    fieldRef:
      fieldPath: metadata.namespace
- name: APP_ENV
  value: {{ required "I need to know my environment" .Values.environment | quote }}
- name: JWT_SECRET_KEY
  value: /secrets/JWT_SECRET_KEY_PEM
- name: JWT_PUBLIC_KEY
  value: /secrets/JWT_PUBLIC_KEY_PEM
{{- end -}}

{{- define "deployment.imagePullSecrets" -}}
{{- range .Values.imagePullSecrets -}}
  - name: {{ . }}
{{- end -}}
{{- end -}}
