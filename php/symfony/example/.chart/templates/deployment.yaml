{{- $fullname := include "app.fullname" . -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "app.fullname" . }}
  labels:
    {{- include "app.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  {{- with .Values.strategy }}
  strategy:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "app.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "app.selectorLabels" . | nindent 8 }}
    spec:
{{- if .Values.nodeSelector }}
      nodeSelector:
{{ toYaml .Values.nodeSelector | indent 10 }}
{{- end }}
{{- if .Values.tolerations }}
      tolerations:
{{ toYaml .Values.tolerations | indent 10 }}
{{- end }}
      imagePullSecrets:
        {{- include "deployment.imagePullSecrets" . | nindent 8 }}
      initContainers:
        - name: init-code
          image: "{{ .Values.php.registry }}/{{ .Values.php.repository }}:{{ .Values.php.tag }}"
          command: ["/bin/sh", "-c", "cp -r /app/.env /app/* /app-volume/"]
          volumeMounts:
            - mountPath: /app-volume
              name: app-volume
          securityContext:
            allowPrivilegeEscalation: false
            runAsGroup: 82
            runAsUser: 82
      containers:
        - name: php
          image: "{{ .Values.php.registry }}/{{ .Values.php.repository }}:{{ .Values.php.tag }}"
          imagePullPolicy: {{ .Values.php.pullPolicy }}
          command: ["php-fpm"]
          env:
            {{- include "app.environment" . | nindent 12 }}
            {{- range $key, $val := .Values.php.env }}
            - name: {{ $key }}
              value: {{ default "" $val | toString | quote }}
            {{- end -}}
            {{- range $key, $val := .Values.php.secrets }}
            - name: {{ $key }}
              valueFrom:
                secretKeyRef:
                  name: {{ $fullname }}
                  key: {{ $key }}
            {{- end -}}
          {{- with .Values.php.livenessProbe }}
          livenessProbe:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.php.readinessProbe }}
          readinessProbe:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.php.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.php.securityContext }}
          securityContext:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          volumeMounts:
            - mountPath: /secrets
              name: secrets
            - mountPath: /app
              name: app-volume
            - name: php-conf-d
              mountPath: /usr/local/etc/php/conf.d
        - name: nginx
          image: "{{ .Values.nginx.registry }}/{{ .Values.nginx.repository }}:{{ .Values.nginx.tag }}"
          imagePullPolicy: {{ .Values.nginx.pullPolicy }}
          command: ["nginx"]
          args: ["-g", "daemon off;"]
          ports:
            - name: http
              containerPort: {{ .Values.nginx.port }}
          {{- with .Values.nginx.livenessProbe }}
          livenessProbe:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.nginx.readinessProbe }}
          readinessProbe:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.nginx.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.nginx.securityContext }}
          securityContext:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: nginx-conf
              mountPath: /etc/nginx/nginx.conf
              subPath: nginx.conf
            - name: app-volume
              mountPath: /app
      volumes:
        - name: secrets
          secret:
            secretName: {{ include "app.fullname" . }}
        - name: nginx-conf
          configMap:
            name: {{ printf "%s-%s" (include "app.fullname" .) "nginx-conf" }}
        - name: php-conf-d
          configMap:
            name: {{ printf "%s-%s" (include "app.fullname" .) "php-conf-d" }}
        - name: app-volume
          emptyDir: {}
